
public class Board {
	private Square[][] tictactoeBoard;
	
	public Board() {
		this.tictactoeBoard = new Square[3][3];
		for(Square[] row : this.tictactoeBoard)
			for(int i = 0; i < row.length; i++)
				row[i] = Square.BLANK;
	}
	
	public void reset() {
		for(Square[] row : this.tictactoeBoard)
			for(int i = 0; i < row.length; i++)
				row[i] = Square.BLANK;
	}
	
	public boolean placeToken(int col, int row, Square token) {
		if(!(col >= 0 && col <= 2) || !(row >= 0 && row <= 2))
			return false;
		if(this.tictactoeBoard[row][col] != Square.BLANK)
			return false;
		this.tictactoeBoard[row][col] = token;
		return true;
	}
	
	public boolean checkIfFull() {
		for(Square[] row : this.tictactoeBoard)
			for(int i = 0; i < row.length; i++)
				if(row[i] == Square.BLANK)
					return false;
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		for(Square[] row : this.tictactoeBoard)
			if(row[0] == playerToken && row[1] == playerToken && row[2] == playerToken)
				return true;
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken){
		for(int i = 0; i < this.tictactoeBoard[0].length; i++)
			if(this.tictactoeBoard[0][i] == playerToken && this.tictactoeBoard[1][i] == playerToken && this.tictactoeBoard[2][i] == playerToken)
				return true;
		return false;
	}
	
	private boolean checkIfWinningDiagonal(Square playerToken) {
		if(this.tictactoeBoard[1][1] == playerToken) {
			if(this.tictactoeBoard[0][0] == playerToken && this.tictactoeBoard[2][2] == playerToken)
				return true;
			else if(this.tictactoeBoard[0][2] == playerToken && this.tictactoeBoard[2][0] == playerToken)
				return true;
		}
		return false;
	}
	
	public boolean checkIfWinning(Square playerToken) {
		return checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken);
	}
	
	public String toString() {
		String displayBoard = "  0 1 2\n";
		for(int i = 0; i < this.tictactoeBoard.length; i++) {
			displayBoard += i;
			for(int j = 0; j < this.tictactoeBoard[i].length; j++)
				displayBoard += " " + this.tictactoeBoard[i][j];
			displayBoard += "\n";
		}
			
		return displayBoard;
	}
}
