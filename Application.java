import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		System.out.println("Welcome to Tic-Tac-Toe!");
		Board board = new Board();
		Scanner kb = new Scanner(System.in);
		boolean gameOver;
		boolean playAgain = true;
		int row, col;
		int player = 1;
		int[] gamesWonByPlayer = {0, 0};
		Square playerToken = Square.X;
		
		do {
			row = -1;
			col = -1;
			gameOver = false;
			
			while(!gameOver) {
				if(player == 1) {
					playerToken = Square.X;
				}else {
					playerToken = Square.O;
				}
				do {
					System.out.println(board);
					System.out.println("Enter the row of your move player " + playerToken);
					if(kb.hasNextInt())
						row = kb.nextInt();
					else
						kb.nextLine();
					System.out.println("Enter the column of your move player " + playerToken);
					if(kb.hasNextInt())
						col = kb.nextInt();
					else
						kb.nextLine();
					
				}while(!board.placeToken(col, row, playerToken));
				
				
				
				if(board.checkIfWinning(playerToken)) {
					System.out.println(board);
					System.out.println("Player " + player + " won");
					gamesWonByPlayer[player - 1]++;
					gameOver = true;
				}else if(board.checkIfFull()) {
					System.out.println(board);
					System.out.println("It's a tie !");
					gameOver = true;
				}else {
					player++;
					if(player > 2)
						player = 1;
				}
			}
			
			System.out.println("Would you like to play again? (Y/N)");
			kb.nextLine();
			if(kb.nextLine().toUpperCase().charAt(0) == 'Y') {
				System.out.println("Starting a new game!");
				board.reset();
			}else {
				System.out.println("Player 1 ("+ Square.X +") won " + gamesWonByPlayer[0] +" times during this session");
				System.out.println("Player 2 (" + Square.O + ") won " + gamesWonByPlayer[1] +" times during this session");
				System.out.println("Exiting game");
				playAgain = false;
			}
		}while(playAgain);
		
	}
}
